<?php

namespace App\Http\Controllers;

use App\Model\Sujet;
use App\Model\Observeur;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ObserveurController extends BaseController
{
    function regUpdate() {
        $a = new Sujet;
        $b = new Observeur('b');
        $c = new Observeur('c');

        $a->regObs($b);
        $a->regObs($c);

        $texteA1 = $b->getTexte();
        $texteB1 = $c->getTexte();

        $a->majObs();

        $texteA2 = $b->getTexte();
        $texteB2 = $c->getTexte();

        return view('observeur', [
            'texteA1' => $texteA1,
            'texteB1' => $texteB1,
            'texteA2' => $texteA2,
            'texteB2' => $texteB2,

        ]);


    }
}