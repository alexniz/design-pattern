<?php

namespace App\Http\Controllers;

use App\Model\Usine;
use App\Model\Concession;
use App\Model\Iterateur;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class IteratorController extends BaseController
{
    function parcourir() {
        $usine = new Usine;
        $voiture1 = $usine::carFactory('Opel');
        $voiture2 = $usine::carFactory('Renault');
        $voiture3 = $usine::carFactory('Opel');
        $voiture4 = $usine::carFactory('Renault');

        $concession = new Concession;
        $concession->addVoiture($voiture1);
        $concession->addVoiture($voiture2);
        $concession->addVoiture($voiture3);
        $concession->addVoiture($voiture4);

        $voituresConcession = [];

        $iterateurVoiture = new Iterateur($concession->getVoiture());
        //$iterateurVoiture = new Iterateur();

        $n = 0;

        //On itère sur les voitures du concessionnaire pour remplir un nouveau tableau à renvoyer
        //vers la vue. 
        while($iterateurVoiture->hasNext()) {
            $voituresConcession['voiture' .strval($n)]= $iterateurVoiture->next()->getMarque();
            $n = $n + 1;
        }

        return view('iterator', $voituresConcession);
        
    }
}
