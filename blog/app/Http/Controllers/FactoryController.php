<?php

namespace App\Http\Controllers;

use App\Model\Usine;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class FactoryController extends BaseController
{
    function fabriquer($marque) {
        $usine = new Usine;
        $voiture = $usine::carFactory($marque);
        $marque = $voiture->getMarque();
        //Avec cette commande on peut récupérer la classe de la voiture
        $classe = get_class($voiture);


        return view('usine', [
            'marque' => $marque,
            'classe' => $classe,
        ]);
    }


    }



