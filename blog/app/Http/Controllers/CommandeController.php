<?php

namespace App\Http\Controllers;

use App\Model\Facture;
use App\Model\Commande;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class CommandeController extends BaseController
{
    function passerCommande($marque) {
        $commande = New Commande;
        $facture = New Facture;

        $facture = $commande->commander($marque);

        $montant = $facture->getMontant();
        $marqueVoiture = $facture->getVoiture()->getMarque();
        $nomConcession = $facture->getConcession()->getNom();
        $classe = get_class($facture);

        return view('facture', [
            'montant' => $montant,
            'marqueVoiture' => $marqueVoiture,
            'nomConcession' => $nomConcession,
            'classe' => $classe,
        ]);
    }


    }



