<?php

namespace App\Http\Controllers;

use App\Model\Voiture;
use App\Model\Opel;
use App\Model\Renault;
use App\Model\FactureLine;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

//Note : Contrôleur pour le DP façade
class FactureLineController extends BaseController
{
    public function ajoutLigne() {
        $voiture1 = new Opel;
        $voiture2 = new Renault;

        $factureLine1 = new Factureline($voiture1);
        $factureLine2 = new Factureline($voiture2);

        $ligne1HT = $factureLine1->getHT();
        $ligne2HT = $factureLine2->getHT();

        $ligne1Rate = $factureLine1->getRate();
        $ligne2Rate = $factureLine2->getRate();

        $ligne1TTC = $factureLine1->getTTC();
        $ligne2TTC = $factureLine2->getTTC();

        return view('factureLigne', [
            'ligne1HT' => $ligne1HT,
            'ligne2HT' => $ligne2HT,
            'ligne1Rate' => $ligne1Rate,
            'ligne2Rate' => $ligne2Rate,
            'ligne1TTC' => $ligne1TTC,
            'ligne2TTC' => $ligne2TTC,
        ]);

    }


}
