<?php

namespace App\Http\Controllers;

use App\Model\Node;
use App\Model\FeuilleVoiture;
use App\Model\ComponentInterface;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class CompositeController extends BaseController {

    public function displayComposite() {
    
        $Voitures = new Node("Voitures");
        $Opels = new Node("Opel");
        $Renaults = new Node ("Renault");

        $Voitures->addChild($Opels);
        $Voitures->addChild($Renaults);

        //Les Opels
        $opelCinquante = new FeuilleVoiture(50);
        $opelCent = new FeuilleVoiture(100);
        
        $Opels->addChild($opelCinquante);
        $Opels->addChild($opelCent);

        //Les Renaults
        $renaultDix = new FeuilleVoiture(10);
        $renaultVintCinq = new FeuilleVoiture(25);
        
        $Renaults->addChild($renaultDix);
        $Renaults->addChild($renaultVintCinq);

        $prixVoitures = $Voitures->getPrix();
        $prixRenaults = $Renaults->getPrix();
        $prixOpelCent = $opelCent->getPrix();

        return view('composite', [
            'prixVoitures' => $prixVoitures,
            'prixRenaults' => $prixRenaults,
            'prixOpelCent' => $prixOpelCent,
        ]);
    
    }

}