<?php

namespace App\Model;

Class ObjectModel {

    public static $instance;

    public $compteur = 0;
    
    public static function getInstance() {
        if(is_null(self::$instance)) {
            self::$instance = new ObjectModel();
        }

        return self::$instance;
    }

    private function __construct() {
        
    }

    public function getCompteur() {
        return $this->compteur;
    }

    public function setCompteur($valeur) {
        $this->compteur = $valeur;
    }

    public function increment() {
        $count = $this->getCompteur();
        $this->setCompteur($count + 1);
    }
}