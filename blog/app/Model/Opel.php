<?php

namespace App\Model;

Class Opel extends Voiture {

    public static $brandName = "Opel";

    public static $montant = 690.42;

    public function __construct() {
        
    }

    public function getMarque() {
        return self::$brandName;
    }
    
    public function getMontant() {
        return self::$montant;
    }
    
}