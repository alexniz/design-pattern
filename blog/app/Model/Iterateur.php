<?php

namespace App\Model;

use phpDocumentor\Reflection\Types\Integer;

class Iterateur {

    
    private $collection = [];
    private $nindex = -1;

    public function __construct(array $collection)
    {
        $this->collection = $collection;
    }

    // public function setNindex(Integer $integer) {
    //     $this->nindex = $integer;
    // }

    public function hasNext(): bool
    {
        $hasNext = isset($this->collection[$this->nindex + 1]);
        return $hasNext;
    }
    
    public function next()
    {
        $this->nindex++;
        return $this->collection[$this->nindex];
    }
}