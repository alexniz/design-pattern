<?php

namespace App\Model;
    
use App\Model\ComponentInterface;
use App\Model\FeuilleVoiture;

    class Node implements ComponentInterface {    

        public $children = [];
        public $nom = null;   

        public function __construct($nom){
            $this->nom = $nom;
        }
        
        public function getPrix() {

            $calculPrix = 0;

            foreach($this->children as $children) 
            {
                $calculPrix += $children->getPrix();
            }
            return $calculPrix;
        }    
    
        public function addChild($child) {
            array_push($this->children, $child);
            return $child;
        }
}