<?php

use App\Model\Concession;
use App\Model\Voiture;



namespace App\Model;

Class Facture {

    public $montant;
    public $voiture;
    public $concession;
    
    public function __construct() {
        
    }

    public function addLigne(Voiture $voiture, Concession $concession) {
        $this->montant = $this->montant + $voiture->getMontant();
        $this->voiture = $voiture;
        $this->concession = $concession;
    }

    public function getMontant() {
        return $this->montant;
    }

    public function getVoiture() {
        return $this->voiture;
    }

    public function getConcession() {
        return $this->concession;
    }


}