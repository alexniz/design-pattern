<?php

namespace App\Model;

Class Renault extends Voiture {

    public static $brandName = "Renault";

    public static $montant = 420.69;

    public function __construct() {
        
    }

    public function getMarque() {
        return self::$brandName;
    }

    public function getMontant() {
        return self::$montant;
    }

    
}