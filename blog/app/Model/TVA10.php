<?php

namespace App\Model;

use App\Model\Concession;
use App\Model\Voiture;

class TVA10 extends TVA  {


    public function __construct() {
        
    }
    
    function execute($p) {
        return $p*1.1;
    }

    function rate(){
        return 1.1;
    }
}