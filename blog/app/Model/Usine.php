<?php

namespace App\Model;

use App\Model\Opel;
use App\Model\Renault;

Class Usine {
    public static function carFactory($string) {

        switch ($string) {
            case 'Opel':
                $voiture = new Opel;
                break;
            case 'Renault':
                $voiture = new Renault;
                break;
            default :
                $voiture = new Opel;
        }
        return $voiture;
    }
}

