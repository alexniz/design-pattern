<?php

namespace App\Model;

use App\Model\Usine;
use App\Model\Facture;
use App\Model\Concession;

Class Commande {

    public function commander(string $marque) {
        $concession = new Concession;
        $usine = new Usine;
        $facture = new Facture;

        $voiture = $usine::carFactory($marque);
        $facture->addLigne($voiture, $concession);
        
        return $facture;
    }


}