<?php

namespace App\Model;

use App\Model\Oberveur;

Class Sujet {

        private $oberveurs = []; 

        public function regObs($observeur) {
            $this->observeurs[$observeur->getId()] = $observeur;
        }

        public function unregObs($observeur) {
            unset($this->observeurs[$observeur->getId()]);
        }

        public function majObs() {
            foreach($this->observeurs as $observeur)
            {
                $observeur->maj();
            }
        }    
    }


