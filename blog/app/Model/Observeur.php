<?php

namespace App\Model;


Class Observeur {

    private $texte = "rien";
    private $nbMaj = 0;
    private $id = null;

    public function __construct($id) {
        $this->id = $id;
        $this->texte = "rien";
        $this->nbMaj = 0;
    }

    public function maj(){
        $this->texte = "Au moins une mise à jour faite";
        $this->nbMaj++;
    }  

    public function getTexte(){
        return $this->texte;
    }

    public function getId(){
        return $this->id;
    }
}