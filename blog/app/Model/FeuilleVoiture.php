<?php

namespace App\Model;

use App\Model\ComponentInterface;

Class FeuilleVoiture implements ComponentInterface {

    private $prix = null;

    public function __construct($prix){
        $this->prix = $prix;
    } 

    public function getPrix() {
        return $this->prix;
    }


}