<?php

namespace App\Model;

use App\Model\TVA10;
use App\Model\TVA30;
use App\Model\TVA;

class FactureLine {

    public $priceHT = 0;
    public $tvaStrat = null;

    public function __construct(Voiture $v) {
        $this->priceHT = $v->getMontant();
        if($v->getMarque() == "Opel") {
            
            $this->tvaStrat = new TVA30;
        } else {
            $this->tvaStrat = new TVA10;
        }
    }

    public function getHT() {
        return $this->priceHT;
    }

    public function getRate() {
        return $this->tvaStrat->rate();
    }

    public function getTTC() {
        return $this->tvaStrat->execute($this->priceHT);
    }


}