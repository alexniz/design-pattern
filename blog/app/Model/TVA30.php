<?php

namespace App\Model;

use App\Model\Concession;
use App\Model\Voiture;

class TVA30 extends TVA  {


    public function __construct() {
        
    }
    
    function execute($p) {
        return $p*1.3;
    }

    function rate(){
        return 1.3;
    }
}