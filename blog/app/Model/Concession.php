<?php

namespace App\Model;

Class Concession {

    public $voitures = array();
    //Nom arbitraire donné à toute instance par défaut, simplement pour vérifier 
    //qu'on est capable de récupérer les attributs d'une concession instanciée
    public $nom = 'Concession de Lannion';

    public function __construct() {
        
    }

    public function addVoiture(Voiture $voiture) {
        $this->voitures[] = $voiture;
    }

    public function getVoiture() {
        return $this->voitures;
    }

    public function getNom() {
        return $this->nom;
    }
}