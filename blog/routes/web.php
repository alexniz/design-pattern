<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('test/{id}', function ($id) {
//     return 'test '.$id;
// });

Route::get('/test', 'TestController@test');
Route::get('/usine/{id}', 'FactoryController@fabriquer');
Route::get('/facture/{id}', 'CommandeController@passerCommande');
Route::get('/iterator', 'IteratorController@parcourir');
Route::get('/factureligne', 'FactureLineController@ajoutLigne');
Route::get('/observeur', 'ObserveurController@regUpdate');
Route::get('/composite', 'CompositeController@displayComposite');






